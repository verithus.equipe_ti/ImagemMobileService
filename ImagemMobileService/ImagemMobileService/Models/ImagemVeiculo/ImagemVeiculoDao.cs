﻿using ImagemMobileService.Models.Conexao;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImagemMobileService.Models.ImagemVeiculo
{
    public class ImagemVeiculoDao
    {
        public int IncluirImagemVeiculo(ImagemVeiculoEntity ImagemVeiculo)
        {
            int retorno = 0;

            string thisMsgErro = string.Empty;
            AcessoBancoDados acesso = new AcessoBancoDados();


            try
            {
                SqlConnection conn = acesso.OpenConnection();
                SqlCommand comm = new SqlCommand();

                comm.Connection = conn;
                comm.CommandType = CommandType.StoredProcedure;
                comm.CommandText = "SPVERITHUS_IncluirImageToDB";


                comm.Parameters.AddWithValue("@PlacaVeiculo", ImagemVeiculo.NumPlaca);
                comm.Parameters.AddWithValue("@NomeImagem", ImagemVeiculo.Nome);
                comm.Parameters.AddWithValue("@Imagem", ImagemVeiculo.Imagem);
                comm.Parameters.AddWithValue("@IdUser", 26);

                object oIdentity = comm.ExecuteScalar();
                if (oIdentity != null)
                    retorno = Convert.ToInt32(oIdentity);

                acesso.CloseConnection();

            }
            catch (Exception ex)
            {
                throw ex;
            }



            return retorno;
        }

    }
}
