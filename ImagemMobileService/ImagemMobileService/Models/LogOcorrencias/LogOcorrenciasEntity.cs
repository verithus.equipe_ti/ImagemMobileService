﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImagemMobileService.Models.LogOcorrencias
{
    public class LogOcorrenciasEntity
    {

        #region Private members

        private long _IdOcorrencia;
        private int _IdTipoKaptor;
        private DateTime _DtOcorrencia;
        private DateTime _DtInicial;
        private DateTime _DtFinal;
        private string _DscOcorrencia = string.Empty;
        private string _DscChavePrimaria = string.Empty;
        private string _DscConteudoChavePrimaria = string.Empty;
        private string _DscCampos = string.Empty;
        private string _DscConteudo = string.Empty;
        private string _IndAcao = string.Empty;

        #endregion Private members

        #region Construtor

        public LogOcorrenciasEntity()
        {

        }

        public LogOcorrenciasEntity(int IdTipoKaptor, string DscOcorrencia, string DscChavePrimaria, string DscConteudoChavePrimaria, string IndAcao, string DscCampos, string DscConteudo)
        {
            this.IdTipoKaptor = IdTipoKaptor;
            this.DtOcorrencia = DateTime.Now;
            this.DscOcorrencia = DscOcorrencia;
            this.DscChavePrimaria = DscChavePrimaria;
            this.DscConteudoChavePrimaria = DscConteudoChavePrimaria;
            this.IndAcao = IndAcao;
            this.DscCampos = DscCampos;
            this.DscConteudo = DscConteudo;
        }


        #endregion Construtor

        #region Public members

        public long IdOcorrencia
        {
            get { return _IdOcorrencia; }
            set { _IdOcorrencia = value; }
        }

        public DateTime DtOcorrencia
        {
            get { return _DtOcorrencia; }
            set { _DtOcorrencia = value; }
        }

        public DateTime DtInicial
        {
            get { return _DtInicial; }
            set { _DtInicial = value; }
        }

        public DateTime DtFinal
        {
            get { return _DtFinal; }
            set { _DtFinal = value; }
        }

        public string DscOcorrencia
        {
            get { return _DscOcorrencia; }
            set { _DscOcorrencia = value; }
        }

        public int IdTipoKaptor
        {
            get { return _IdTipoKaptor; }
            set { _IdTipoKaptor = value; }
        }


        public string DscChavePrimaria
        {
            get { return _DscChavePrimaria; }
            set { _DscChavePrimaria = value; }
        }

        public string DscConteudoChavePrimaria
        {
            get { return _DscConteudoChavePrimaria; }
            set { _DscConteudoChavePrimaria = value; }
        }

        public string DscCampos
        {
            get { return _DscCampos; }
            set { _DscCampos = value; }
        }

        public string DscConteudo
        {
            get { return _DscConteudo; }
            set { _DscConteudo = value; }
        }

        public string IndAcao
        {
            get { return _IndAcao; }
            set { _IndAcao = value; }
        }


        #endregion Public members

    }


}
