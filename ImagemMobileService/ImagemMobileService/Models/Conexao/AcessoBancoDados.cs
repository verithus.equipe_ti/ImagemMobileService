﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Text;
using System.Threading.Tasks;


namespace ImagemMobileService.Models.Conexao
{
    class AcessoBancoDados
    {
        public string erroSql;
        public string connString;
        private ConnectionString objconn;
        private SqlConnection conn = null;

        public AcessoBancoDados()
        {
            AcessoXml objXml = new AcessoXml();

            objconn = objXml.RetornaConnectionString("contingencia");

            connString = @"Data Source=" + objconn.DataSource + ";Initial Catalog=" + objconn.InitialCatalog + ";User ID=" + objconn.User + ";Password=" + objconn.Password;
        }

        public SqlConnection OpenConnection()
        {
            try
            {
                if (conn == null)
                {
                    conn = new SqlConnection(connString);
                    conn.Open();
                    return conn;
                }

                //conn.Open();
                switch (conn.State)
                {
                    case ConnectionState.Broken:
                        break;
                    case ConnectionState.Closed:
                        conn = new SqlConnection(connString);
                        conn.Open();
                        break;
                    case ConnectionState.Connecting:
                        while (conn.State == ConnectionState.Connecting)
                            Thread.Sleep(500);
                        break;
                    case ConnectionState.Executing:
                        while (conn.State == ConnectionState.Executing)
                            Thread.Sleep(500);
                        break;
                    case ConnectionState.Fetching:
                        while (conn.State == ConnectionState.Fetching)
                            Thread.Sleep(500);
                        break;
                    case ConnectionState.Open:
                        break;
                    default:
                        break;
                }

            }
            catch (SqlException ex)
            {
                conn = null;
                erroSql = ex.ToString();
            }

            return conn;
        }

        public void CloseConnection()
        {
            if (conn != null)
            {
                conn.Close();
            }
        }
    }
}
