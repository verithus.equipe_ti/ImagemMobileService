﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Data;
using ImagemMobileService.Models.Conexao;

namespace ImagemMobileService.Models
{
    public class ImagemMobileDao
    {

        public IList<ImagemMobileEntity> RetornaImagens()
        {
            ImagemMobileEntity RetornoAux = new ImagemMobileEntity();
            IList<ImagemMobileEntity> LstRetorno = new List<ImagemMobileEntity>();
            AcessoBancoDados acesso = new AcessoBancoDados();


            try
            {
                SqlConnection conn = acesso.OpenConnection();
                SqlCommand comm = new SqlCommand();
                comm.Connection = conn;
                comm.CommandType = CommandType.StoredProcedure;
                comm.CommandText = "SPVERITHUS_RetornaImagensMobile";


                // comm.Parameters.AddWithValue("@NumPlaca", imagem.PlacaVeiculo);

                SqlDataAdapter sda = new SqlDataAdapter(comm);
                DataSet ds = new DataSet();

                sda.Fill(ds);

                foreach (DataRow Linha in ds.Tables[0].Rows)
                {
                    RetornoAux = new ImagemMobileEntity();
                    RetornoAux.IdImagem = (int)Linha["IdImagem"];
                    RetornoAux.Imagem = Linha["Imagem"].ToString();
                    RetornoAux.NumPlaca = Linha["numplaca"].ToString();
                    RetornoAux.ImagemVistoria.IdCliente = (int)Linha["IdCliente"];
                    RetornoAux.ImagemVistoria.IdRoteiro = (int)Linha["IdRoteiro"];
                    RetornoAux.ImagemVistoria.IdSinistro = (int)Linha["IdSinistro"];
                    RetornoAux.ImagemVistoria.ImagemVeiculo.NumPlaca = Linha["numplaca"].ToString();
                    RetornoAux.ImagemVistoria.ImagemVeiculo.Imagem = Convert.FromBase64String(Linha["Imagem"].ToString());


                    LstRetorno.Add(RetornoAux);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                acesso.CloseConnection();
            }
            return LstRetorno;
        }



        public IList<ImagemMobileEntity> RetornaImagensSalvados()
        {
            ImagemMobileEntity RetornoAux = new ImagemMobileEntity();
            IList<ImagemMobileEntity> LstRetorno = new List<ImagemMobileEntity>();
            AcessoBancoDados acesso = new AcessoBancoDados();


            try
            {
                SqlConnection conn = acesso.OpenConnection();
                SqlCommand comm = new SqlCommand();
                comm.Connection = conn;
                comm.CommandType = CommandType.StoredProcedure;
                comm.CommandText = "SPVERITHUS_RetornaImagensMobileSalvados";


                // comm.Parameters.AddWithValue("@NumPlaca", imagem.PlacaVeiculo);

                SqlDataAdapter sda = new SqlDataAdapter(comm);
                DataSet ds = new DataSet();

                sda.Fill(ds);

                foreach (DataRow Linha in ds.Tables[0].Rows)
                {
                    RetornoAux = new ImagemMobileEntity();
                    RetornoAux.IdImagem = (int)Linha["IdImagem"];
                    RetornoAux.Imagem = Linha["Imagem"].ToString();
                    RetornoAux.NumPlaca = Linha["numplaca"].ToString();
                    RetornoAux.ImagemVistoria.IdCliente = (int)Linha["IdCliente"];
                    RetornoAux.ImagemVistoria.IdLaudo = (int)Linha["IdLaudo"];
                    RetornoAux.ImagemVistoria.IdSinistro = (int)Linha["IdSinistro"];
                    RetornoAux.ImagemVistoria.ImagemVeiculo.NumPlaca = Linha["numplaca"].ToString();
                    RetornoAux.ImagemVistoria.ImagemVeiculo.Imagem = Convert.FromBase64String(Linha["Imagem"].ToString());


                    LstRetorno.Add(RetornoAux);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                acesso.CloseConnection();
            }
            return LstRetorno;
        }


        public void AlterarImagemMobile(ImagemMobileEntity ImagemMobile)
        {
            //int retorno = 0;


            AcessoBancoDados acesso = new AcessoBancoDados();


            try
            {
                SqlConnection conn = acesso.OpenConnection();
                SqlCommand comm = new SqlCommand();

                comm.Connection = conn;
                comm.CommandType = CommandType.StoredProcedure;
                comm.CommandText = "SPVERITHUS_AlterarImagemMobile";


                comm.Parameters.AddWithValue("@IdImagem", ImagemMobile.IdImagem);
                comm.Parameters.AddWithValue("@IndSucesso", ImagemMobile.IndIncluido);

                comm.ExecuteScalar();

                acesso.CloseConnection();

            }
            catch (Exception ex)
            {
                throw ex;
            }



            //return retorno;
        }


        //public ImagemEntity IncluirImagem(byte[] imagem)
        //{
        //    ImagemEntity RetornoAux = new ImagemEntity();
        //    IList<ImagemEntity> LstRetorno = new List<ImagemEntity>();
        //    AcessoBancoDados acesso = new AcessoBancoDados();

        //    try
        //    {
        //        SqlConnection conn = acesso.OpenConnection();
        //        SqlCommand comm = new SqlCommand();
        //        comm.Connection = conn;
        //        comm.CommandType = CommandType.StoredProcedure;
        //        comm.CommandText = "SPVERITHUS_IncluirTesteImagem";

        //        comm.Parameters.AddWithValue("@NumPlaca", "pgc8701");
        //        comm.Parameters.AddWithValue("@NomeImagem", "teste");
        //        comm.Parameters.AddWithValue("@Imagem", imagem);

        //        object oIdentity = comm.ExecuteScalar();
        //        SqlDataAdapter sda = new SqlDataAdapter(comm);
        //        DataSet ds = new DataSet();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        acesso.CloseConnection();
        //    }
        //    return null;

        //}
    }
}
